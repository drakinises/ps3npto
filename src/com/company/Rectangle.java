package com.company;

public class Rectangle {
    Point A;
    Point B;
    Point C;
    Point D;

    public Rectangle() {
    }

    public Rectangle(Rectangle rect) {
        this.A = new Point(rect.A);
        this.B = new Point(rect.B);
        this.C = new Point(rect.C);
        this.D = new Point(rect.D);
    }

    public Rectangle(Point a, Point b, Point c, Point d) {
        this.A = a;
        this.B = b;
        this.C = c;
        this.D = d;
    }


    void move(double dx, double dy){
        A.move(dx, dy);
        B.move(dx, dy);
        C.move(dx, dy);
        D.move(dx, dy);
    }
    @Override
    public String toString() {
        return "Rectangle: A = " + A.toString() + " B = " + B.toString() + " C = " + C.toString() + " D = " + D.toString();
    }

    double getArea(){
        double a, b;
        a = Math.sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
        b = Math.sqrt((B.x - C.x) * (B.x - C.x) + (B.y - C.y) * (B.y - C.y));
        return a*b;
    }

}
