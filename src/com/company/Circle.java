package com.company;

public class Circle {
    Point p;
    double r;

    public Circle(Point o, double r) {
        this.p = o;
        this.r = r;
    }

    public Circle() {
    }

    public Circle(Circle circle) {
        this.p = new Point(circle.p);
        this.r = circle.r;
    }

    void move(double dx, double dy){
        p.move(dx, dy);
    }
    @Override
    public String toString() {
        return "Circle: p = " + p.toString() + " r = " + r;
    }
    double getArea(){
        return 3.14*r*r;
    }
}

