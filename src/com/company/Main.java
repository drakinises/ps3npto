package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	Picture picture = new Picture();
	menu(picture);
    }

    static void menu(Picture picture){
        Point p1;
        Point p2;
        Point p3;
        Point p4;
        int input;
        int x, y, x2, y2, x3, y3, x4, y4;
        Scanner sc = new Scanner(System.in);
        System.out.println("Wybierz czynność, którą chcesz wykonać: ");
        System.out.println("Aby dodać nowy punkt wpisz 1.");
        System.out.println("Aby dodać nowy odcinek wpisz 2.");
        System.out.println("Aby dodać nowy okrąg wpisz 3.");
        System.out.println("Aby dodać nowy prostokąt wpisz 4.");
        System.out.println("Aby wyświetlić obiekt wpisz 5.");
        System.out.println("Aby przesunąć punkt wpisz 6.");
        System.out.println("Aby przesunąć odcinek wpisz 7.");
        System.out.println("Aby przesunąć okrąg wpisz 8.");
        System.out.println("Aby przesunąć prostokąt wpisz 9.");
        System.out.println("Aby wyświetlić sumę pól wpisz 10.");
        System.out.println("Aby wyjść wpisz 11.");
        input = sc.nextInt();
        switch (input){
            case 1:
                System.out.println("Podaj współrzędną x: ");
                x = sc.nextInt();
                System.out.println("Podaj współrzędną y: ");
                y = sc.nextInt();
                p1 = new Point(x, y);
                picture.add(p1);
                break;
            case 2:
                System.out.println("Podaj współrzędną x pierwszego punktu: ");
                x = sc.nextInt();
                System.out.println("Podaj współrzędną y pierwszego punktu: ");
                y = sc.nextInt();
                System.out.println("Podaj współrzędną x drugiego punktu: ");
                x2 = sc.nextInt();
                System.out.println("Podaj współrzędną y drugiego punktu: ");
                y2 = sc.nextInt();
                p1 = new Point(x,y);
                p2 = new Point(x2,y2);
                Section s = new Section(p1, p2);
                picture.add(s);
                break;
            case 3:
                System.out.println("Podaj współrzędną x środka: ");
                x = sc.nextInt();
                System.out.println("Podaj współrzędną y środka: ");
                y = sc.nextInt();
                System.out.println("Podaj współrzędną promień: ");
                x2 = sc.nextInt();
                p1 = new Point(x, y);
                Circle c = new Circle(p1, x2);
                picture.add(c);
                break;
            case 4:
                System.out.println("Podaj współrzędną x pierwszego punktu: ");
                x = sc.nextInt();
                System.out.println("Podaj współrzędną y pierwszego punktu: ");
                y = sc.nextInt();
                System.out.println("Podaj współrzędną x drugiego punktu: ");
                x2 = sc.nextInt();
                System.out.println("Podaj współrzędną y drugiego punktu: ");
                y2 = sc.nextInt();
                System.out.println("Podaj współrzędną x trzeciego punktu: ");
                x3 = sc.nextInt();
                System.out.println("Podaj współrzędną y trzeciego punktu: ");
                y3 = sc.nextInt();
                System.out.println("Podaj współrzędną x czwartego punktu: ");
                x4 = sc.nextInt();
                System.out.println("Podaj współrzędną y czwartego punktu: ");
                y4 = sc.nextInt();
                p1 = new Point(x,y);
                p2 = new Point(x2,y2);
                p3 = new Point(x3,y3);
                p4 = new Point(x4,y4);
                Rectangle r = new Rectangle(p1, p2, p3, p4);
                picture.add(r);
                break;
            case 5:
                System.out.println(picture.toString());
                break;
            case 6:
                System.out.println("Podaj indeks punktu, który chcesz przesunąć");
                x2 = sc.nextInt();
                if (x2>=picture.point.size()){
                    System.out.print("Niepoprawny indeks");
                    menu(picture);
                }
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość x ");
                x = sc.nextInt();
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość y");
                y = sc.nextInt();
                picture.point.get(x2).move(x, y);
                break;
            case 7:
                System.out.println("Podaj indeks odcinka, który chcesz przesunąć");
                x2 = sc.nextInt();
                if (x2>picture.section.size()){
                    System.out.print("Niepoprawny indeks");
                    menu(picture);
                }
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość x ");
                x = sc.nextInt();
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość y");
                y = sc.nextInt();
                picture.section.get(x2).move(x, y);
                break;
            case 8:
                System.out.println("Podaj indeks koła, które chcesz przesunąć");
                x2 = sc.nextInt();
                if (x2>picture.circle.size()){
                    System.out.print("Niepoprawny indeks");
                    menu(picture);
                }
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość x ");
                x = sc.nextInt();
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość y");
                y = sc.nextInt();
                picture.circle.get(x2).move(x, y);
                break;
            case 9:
                System.out.println("Podaj indeks prostokąta, który chcesz przesunąć");
                x2 = sc.nextInt();
                if (x2>picture.rectangle.size()){
                    System.out.print("Niepoprawny indeks");
                    menu(picture);
                }
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość x ");
                x = sc.nextInt();
                System.out.println("Podaj wektor o jaki ma byc przesunięta wartość y");
                y = sc.nextInt();
                picture.rectangle.get(x2).move(x, y);
                break;
            case 10:
                System.out.println(picture.sum());
                break;
            case 11:
                return;
            default:
                System.out.println("Podaj poprawną liczbę");

        }
        menu(picture);
    }
}
