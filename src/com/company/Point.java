package com.company;

public class Point {
    double x;
    double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    public Point(Point point) {
        this.x = point.x;
        this.y = point.y;
    }
    void move(double dx, double dy){
        this.x = this.x + dx;
        this.y = this.y + dx;
    }
    @Override
    public String toString() {
        return "Point: x = " + x + " y = " + y;
    }

    double getArea(){
        return 0;
    }
}
