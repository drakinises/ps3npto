package com.company;

import java.util.ArrayList;

public class Picture {

    ArrayList <Point> point = new ArrayList<Point>();
    ArrayList <Section> section = new ArrayList<Section>();
    ArrayList <Circle> circle = new ArrayList<Circle>();
    ArrayList <Rectangle> rectangle = new ArrayList<Rectangle>();

    void add (Point p){
        point.add(p);
    }
    void add(Section s){
        section.add(s);
    }
    void add(Circle c){
        circle.add(c);
    }
    void add(Rectangle r){
        rectangle.add(r);
    }

    @Override
    public String toString() {
        String output = "";
        for (Point p: point) {
            output = output + p.toString() + "\n";
        }
        for (Section s: section) {
            output = output + s.toString() + "\n";
        }
        for (Circle c: circle) {
            output = output + c.toString() + "\n";
        }
        for (Rectangle r: rectangle) {
            output = output + r.toString() + "\n";
        }
        return output;
    }
    double sum(){
        double output = 0;
        for (Point p: point) {
            output = output + p.getArea();
        }
        for (Section s: section) {
            output = output + s.getArea();
        }
        for (Circle c: circle) {
            output = output + c.getArea();
        }
        for (Rectangle r: rectangle) {
            output = output + r.getArea();
        }
        return output;
    }
}
