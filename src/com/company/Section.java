package com.company;

public class Section {
    Point p1;
    Point p2;

    public Section(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Section(Section sect) {
        this.p1 = new Point(sect.p1);
        this.p2 = new Point(sect.p2);
    }

    void move(double dx, double dy){
        p1.move(dx, dy);
        p2.move(dx, dy);
    }

    @Override
    public String toString() {
        return "Section: p1 = " + p1.toString() + " p2 = " + p2.toString();
    }
    double getArea(){
        return 0;
    }
}
